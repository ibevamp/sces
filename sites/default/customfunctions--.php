<?php
function recent_users(){
	
	global $user;

	//Counting Views
    $path = $_GET['q'];
    $path_alias = drupal_get_path_alias($_GET["q"]);

	
	$query = db_select('users', 'u');
	$query->condition('u.uid', 0, '<>');
    $query->fields('u', array('uid','name'));

    $result = $query->execute();
    
    while($record = $result->fetchAssoc()) {
         echo($record['uid']."\n\t\n"."<a href =".$path_alias."?uid=".$record['uid'].">".$record['name']."</a>"."<br />");
    }

	

	if( $_GET['uid'] )
	{
		$counter = 0;
		$total_downloads = 0;
	 //    drupal_set_message($path);
		// $visits = db_query("SELECT count(*) FROM {accesslog} where path = :path", array(':path' => $path))->fetchField();
		// echo "Views from conventional Ways: " . $visits;

		
		// echo just query2 to know how query is parsed.  
	    
	    
	    //Main Title and miscellaneous Information

	    $result_user = db_query("
		  	SELECT
			  f.field_full_n_value AS fullname, 
			  u.mail AS email, 
			  u.created AS joindate,
			  u.login AS lastlogin
			FROM
			  {field_data_field_full_n} f, {users} u where f.entity_id = u.uid AND u.uid = :uid
			",
			array(':uid'=>$_GET['uid'])) -> fetchAll();
		        
       // Finding all the Browsers Used
	   $query_all_browsers = db_select('login_activity', 'a')
			//->condition('a.uid', $user->uid, '=')
			->condition('a.uid', $_GET['uid'], '=')
			->fields('a', array('host_user_agent'));

		$result_b = $query_all_browsers->execute();

			
		echo "<div class='data-container' id='profile-info'>";

			echo "<div class='left'>";

				foreach($result_user as $r) {

			    	echo "<h1>".$r->fullname."</h1>";
			    	echo "<span> Email: ".$r->email."</span><br />";
			    	echo "<span> Joining Date: ".date("F jS, Y  \( h:i a \)", $r->joindate)."</span><br />";
			    	echo "<span> Last Login: ".date("F jS, Y  \( h:i a \)", $r->lastlogin)."</span><br />";
			    	echo "<span> Internet Browsers Used: ";
			    	 while($record_all_browsers = $result_b->fetchAssoc()) {
				         $detail_arr = getBrowser($record_all_browsers['host_user_agent']);
				         echo("<img src ='".$detail_arr['icon']."' width='24' height='24' />"." / "."<img src ='".$detail_arr['os_icons']."' width='24' height='24' />"."<br />");
				    }	
			    	echo "</span>";	
			    }
			echo "</div>";

			echo "<div class='right'>";

			echo "</div>";

			echo "<div class='clear'></div>";

		echo "</div>";		
		// Count the number of times the current logged in user has 
		// visited the current page
	    $query_count_visits = db_select('accesslog', 'a')
			//->condition('a.uid', $user->uid, '=')
			->condition('a.uid', $_GET['uid'], '=')
			->condition('a.path', $path, '=')
			->fields('a', array('uid', 'title'))
			->execute()	
			->rowCount();


		
		// Finding all the Pages visited by the logged in User.
		// Also Count the number of times each Unique page is visited.

		$result = db_query("
		  	SELECT
			  a.url, a.title, a.uid,
			  COUNT(*) AS times
			FROM
			  {accesslog} a where a.uid = :uid
			GROUP BY
			  a.url",
			array(':uid'=>$_GET['uid'])) -> fetchAll();
		        
        echo "<div id='page-wrap'>"; 
			echo "\n<table id='hor-minimalist-b'>";
			
			echo "\n\n<thead>";
			echo "\n\n\n<tr>";
			echo "\n\n\n\n<th>Pages Visited</th>";
			echo "\n\n\n\n<th>Times</th>";
			echo "\n\n\n\n<th>Browser</th>";
			echo "\n\n\n</tr>";
			echo "\n\n</thead>";
			
			echo "\n\n<tbody>";

			    foreach($result as $r) {
			    	echo "<tr>"."<td>".$r->url."</td>"."<td>".$r->times." times"."</td>"."<td>"."Browser"."</td>"."</tr>";
			    }
			

			echo "\n\n</tbody>";
			
			echo "\n</table>";
		echo "</div>"; 		




		// Documents Downloaded

		$result_dl = db_query("
		  	SELECT
			  d.name, d.url, n.title, d.count
			FROM
			  {pubdlcnt} d, {node} n where n.nid = d.nid AND d.uid = :uid
			",
			array(':uid'=>$_GET['uid'])) -> fetchAll();
		        
       
		foreach($result_dl as $r) {

	    	$counter ++;
	    	$total_downloads += $r->count;
	    }



		/* Download Numbers PIE Graph */	
		echo "<div class='data-container'>";

			echo "<div class='left'>";

				echo "<h1>Documents Downloaded</h1>";
				echo "<p>Total Documents downloaded: ".$counter." </p>";
				echo "<p>Total Downloaded Count: ".$total_downloads."</p>";

				echo "<div id='page-wrap'>"; 

					echo "\n<table id='hor-minimalist-c'>";
					
					echo "\n\n<thead>";
						echo "\n\n\n<tr>";
							echo "\n\n\n\n<th>Document Name</th>";
							echo "\n\n\n\n<th>Page</th>";
							echo "\n\n\n\n<th>Type</th>";
							echo "\n\n\n\n<th>Count</th>";
					echo "\n\n\n</tr>";
					echo "\n\n</thead>";
					
					echo "\n\n<tbody>";

					    foreach($result_dl as $r) {

					    	echo "<tr>"."<td>".preg_replace("/\\.[^.\\s]{3,4}$/", "", str_replace("_", " ", $r->name))."</td>"."<td>".$r->title."</td>"."<td>".pathinfo($r->name, PATHINFO_EXTENSION)."</td>"."<td>".$r->count."</td>"."</tr>";
				
					    }
					

					echo "\n\n</tbody>";
					
					echo "\n</table>";

				echo "</div>"; 		


			echo "</div>";	

			
			/* Pie Graph JS */
			echo "<script type='text/javascript' src='https://www.google.com/jsapi'></script>";
		    
		    echo "<script type='text/javascript'>";
		    	echo "  google.load('visualization', '1', {packages: ['corechart']})";
		    echo "</script>";

		    echo "<script type='text/javascript'>";
		    	
		    	echo "function drawVisualization() {";
		        	
		        	echo "var options = { ";
						echo "	  width: 450,";
						echo "    height: 450,";
						echo "    chartArea:{left:10,top:10,width:'100%'},";
						//echo "	  title: 'Toppings I Like On My Pizza',";
						echo "    colors: ['#193153', '#919b02', '#9c2b11', '#e5760a', '#f6c7b6']";
					echo "};"; 
					//echo "var options = {'title':'asdasdasd asd asd ','width':500,'height':450,'chartArea':{left:0,top:10,width:'100%'}}";
		        	echo "var data = google.visualization.arrayToDataTable([";
		          	echo "['Document', 'No. of Downloads'],";
		        	
		        	foreach($result_dl as $r) {  	
			          	$string = (strlen($r->name) > 55) ? substr($r->name,0,52).'...' : $r->name;
			          	echo "['".preg_replace("/\\.[^.\\s]{3,4}$/", "", str_replace("_", " ", $string))."', ".$r->count."],";
			         }
		        echo "]);";
		      
		        
		        echo "new google.visualization.PieChart(document.getElementById('visualization')).";
		            echo "draw(data, options); ";
		     echo " }";
		      

		      echo "google.setOnLoadCallback(drawVisualization);";
		    echo "</script>";
		    /* </Pie Graph JS */

			echo "<div class='right'>";
				
				echo "<div id='holder'>";

					/* Pie Graph Code */
					echo "<div id='visualization'></div>";	
				
				echo "</div>";

			echo "</div>";
			echo "<div class='clear'></div>";

			



			
		  			

		echo "</div>";





	  	echo "<br />"."Total number of Visits on all Pages: " . "1234" . "<br />";


	}
}
function getBrowser($useragent) {

    // if (isset($_SERVER["HTTP_USER_AGENT"]) OR ($_SERVER["HTTP_USER_AGENT"] != "")) {
    //     $visitor_user_agent = $_SERVER["HTTP_USER_AGENT"];
    // } else {
    //     $visitor_user_agent = "Unknown";
    // }
    
    // check which browser you're using....
	
	if (preg_match('|MSIE ([0-9].[0-9]{1,2})|',$useragent,$matched)) {
		 $browser_version=$matched[1];
		 $browser = 'IE';
		 $icon =  $GLOBALS['base_url'] ."/". path_to_theme().'/images/icons/ie.png';
	 } elseif (preg_match('|Opera/([0-9].[0-9]{1,2})|',$useragent,$matched)) {
		 $browser_version=$matched[1];
		 $browser = 'Opera';
		 $icon =  $GLOBALS['base_url'] ."/". path_to_theme().'/images/icons/opera.png';
	 } elseif(preg_match('|Chrome/([0-9\.]+)|',$useragent,$matched)) {
		 $browser_version=$matched[1];
		 $browser = 'Chrome';
		 $icon =  $GLOBALS['base_url'] ."/". path_to_theme().'/images/icons/chrome.png';
	 } elseif(preg_match('|Firefox/([0-9\.]+)|',$useragent,$matched)) {
		 $browser_version=$matched[1];
		 $browser = 'Firefox';
		 $icon =  $GLOBALS['base_url'] ."/". path_to_theme().'/images/icons/firefox2.png';
	 } elseif(preg_match('|Version/([0-9\.]+)|',$useragent,$matched)) {
		/*
		matches chrome and safari since they are basically the same,
		you can add a check for chrome by simply copy pasting
		and typing 'chrome' rather than safari
		*/
		 $browser_version=$matched[1];
		 $browser = 'Safari';
		 $icon =  $GLOBALS['base_url'] ."/". path_to_theme().'/images/icons/safari.png';
	 } else {
		 // browser not recognized!
		 $browser_version = 0;
		 $browser= 'other';
		 $icon = $GLOBALS['base_url'] ."/". path_to_theme().'/images/icons/unknown.png';
	}
	 //os
	 if (strstr($useragent,'Win')) {
	 	$os='Win';
	 	$os_icons = $GLOBALS['base_url'] ."/". path_to_theme().'/images/icons/windows3.png';
	 } else if ((strstr($useragent,'Mac')) && (!(strstr($useragent,'Mobile')))) {
	 	$os='Mac';
	 	$os_icons = $GLOBALS['base_url'] ."/". path_to_theme().'/images/icons/mac3.png';
	 } else if ((strstr($useragent,'iPad')) && (strstr($useragent,'Mobile'))) {
	 	$os='iPad';
	 	$os_icons = $GLOBALS['base_url'] ."/". path_to_theme().'/images/icons/ipad2.png';
	 } else if (strstr($useragent,'Linux')) {
	 	$os='Linux';
	 	$os_icons = $GLOBALS['base_url'] ."/". path_to_theme().'/images/icons/linux.png';
	 } else if (strstr($useragent,'Unix')) {
	 	$os='Unix';
	 	$os_icons = $GLOBALS['base_url'] ."/". path_to_theme().'/images/icons/linux.png';
	 } else {
	 	$os='Other';
	 	$os_icons = $GLOBALS['base_url'] ."/". path_to_theme().'/images/icons/unknown.png';
	 }

	 return array(
        'name' => $browser,
        'icon' => $icon,
        'version' => $browser_version,
        'os' => $os,
        'os_icons' => $os_icons
    );
}

?>
<?php
function recent_users(){
	
	global $user;

	// Check to see if $user has the administrator role.
	if (!(in_array('administrator', array_values($user->roles)))) {
		if($user->uid)
			echo "You need to be an Administrator to see this page.";

		exit();
	}
	//Counting Views
    $path = $_GET['q'];
    $differential = $_GET['uid'];
    $path_alias = drupal_get_path_alias($_GET["q"]);


    /* Pie Graph JS */
	echo "<script type='text/javascript' src='https://www.google.com/jsapi'></script>";
    
    echo "<script type='text/javascript'>";
    	echo "  google.load('visualization', '1', {packages: ['corechart']});";
   	echo "</script>";


	if($differential)
	{
		$counter = 0;
		$total_downloads = 0;

	    
	    //Main Title and miscellaneous Information

	    $result_user = db_query("
		  	SELECT
			  f.field_full_n_value AS fullname, 
			  u.name AS username,
			  u.mail AS email, 
			  u.created AS joindate,
			  u.login AS lastlogin
			FROM
			  {field_data_field_full_n} f, {users} u where f.entity_id = u.uid AND u.uid = :uid
			",
			array(':uid'=>$_GET['uid'])) -> fetchAll();
		        
       // Finding all the Browsers Used
	   $query_all_browsers = db_select('login_activity', 'a')
			//->condition('a.uid', $user->uid, '=')
			->condition('a.uid', $_GET['uid'], '=')
			->distinct()
			->fields('a', array('host_user_agent'));

		$result_b = $query_all_browsers->execute();

		/*
		$result = db_query("
		  	SELECT
			  a.url, a.title, a.uid,
			  COUNT(*) AS times
			FROM
			  {accesslog} a where a.uid = :uid
			GROUP BY
			  a.url",
			array(':uid'=>$_GET['uid'])) -> fetchAll();
		*/
		$result = db_query("
		  	SELECT
			  n.title, n.nid,
			  COUNT(*) AS times
			FROM
			  {node_view_count} a, {node} n where a.nid = n.nid AND a.uid = :uid
			GROUP BY
			  n.title
			ORDER BY
			  times desc",
			array(':uid'=>$_GET['uid'])) -> fetchAll();


		echo "<div class='data-container' id='profile-info'>";
			echo "<span><a href=".$GLOBALS['base_url'] ."/"."user-statistics>&larr; Back to Main Analytics Page</a></span>";
		echo "</div>";
		
		echo "<div class='data-container'>";
			
			echo "<div class='left' style='width: 40%';>";

				foreach($result_user as $r) {


					$date = date('Y-m-d H:i:s', $r->joindate);
					$dtz = 'America/New_York';
					$joinTimestamp = getNoteDateTimeZone($date, 'US/Central', $dtz);

					$ldate = date('Y-m-d H:i:s', $r->lastlogin);
					$loginTimestamp = getNoteDateTimeZone($ldate, 'US/Central', $dtz);

			    	echo "<h1>".$r->fullname."</h1>";
			    	echo "<span> Username: </span><span class='content-span'>".$r->username."</span><br />";
			    	echo "<span> Joining Date: </span><span class='content-span'>".date("F jS, Y  \( h:i a \)", strtotime($joinTimestamp))."</span><br />";
			    	echo "<span> Last Login: </span><span class='content-span'>".date("F jS, Y  \( h:i a \)", strtotime($loginTimestamp))."</span><br /><br />";
			    	echo "<span style='border-bottom: 1px solid #adadad; padding-bottom:5px; margin-bottom: 8px;'> Browsing History: </span><span class='content-span'><br />";
			    	 
			    	echo "<table id='ver-minimalist' summary='Browsing History'>";
	    		
    					echo "<tbody>";
				    	 while($record_all_browsers = $result_b->fetchAssoc()) {

					         $detail_arr = getBrowser($record_all_browsers['host_user_agent']);
					         echo "<tr>";
					         	echo("<td><img src ='".$detail_arr['icon']."' width='20' height='20' />"." | "."<img src ='".$detail_arr['os_icons']."' width='24' height='24' /> (".$detail_arr['name'].", Version ".$detail_arr['version']." on ".$detail_arr['os'].")  </td>");
					         echo "</tr>"; 
					    }
					    echo "</tbody>";
					echo "</table>";	
			    	echo "</span>";	
			    }
			echo "</div>";


			/* <Pie Graph JS */
		    echo "<script type='text/javascript'>";
		    	
		    	echo "function drawVisualization_pages_visited() {";
		        	
		        	echo "var options = { ";
						echo "	  width: 550,";
						echo "    height: 550,";
						echo "    chartArea:{left:7,top:5, width:'100%'},";
						echo "    fontName: 'Open Sans',";
						echo "    tooltip: { textStyle: { fontName: 'Tahoma', fontSize: 11 } },";
						echo "    colors: [ '#d8b71a', '#193153', '#9c2b11', '#e5760a', '#1d83ae', '#919b02', '#097092', '#ddb928', '#890c0c', '#5c6677', '#0fa7ad', '#ad560f', '#d41473' ]";
						
					echo "};"; 
					//echo "var options = {'title':'asdasdasd asd asd ','width':500,'height':450,'chartArea':{left:0,top:10,width:'100%'}}";
		        	echo "var data = google.visualization.arrayToDataTable([";
		          	echo "['Pages Visited', 'No. of Visits'],";
		        	
		        	foreach($result as $r) {  	
			          	//echo "['".$r->title."', ".$r->times."],";
			          	$resultstr_pages_visited[] = "['".$r->title."', ".$r->times."]";
			         }
			         echo implode("," , $resultstr_pages_visited);
		        echo "]);";
		      
		        
		        echo "new google.visualization.PieChart(document.getElementById('visualization_visit_graph')).";
		            echo "draw(data, options); ";
		     echo " }";
		      

		      echo "google.setOnLoadCallback(drawVisualization_pages_visited);";
		    echo "</script>";
		    /* </Pie Graph JS */


			echo "<div class='right' style='width: 50%';>";

				echo "<div id='holder'>";

					/* Pie Graph Code */
					echo "<div id='visualization_visit_graph'></div>";	
				
				echo "</div>";

			echo "</div>";

			echo "<div class='clear'></div>";

		echo "</div>";		
		// Count the number of times the current logged in user has 
		// visited the current page
	    $query_count_visits = db_select('accesslog', 'a')
			//->condition('a.uid', $user->uid, '=')
			->condition('a.uid', $_GET['uid'], '=')
			->condition('a.path', $path, '=')
			->fields('a', array('uid', 'title'))
			->execute()	
			->rowCount();


		
		// Finding all the Pages visited by the logged in User.
		// Also Count the number of times each Unique page is visited.



        echo "<div id='page-wrap' class='content_scroll'>"; 
			echo "\n<table id='hor-minimalist-b'>";
			
			echo "\n\n<thead>";
			echo "\n\n\n<tr>";
			echo "\n\n\n\n<th>Pages Visited</th>";
			echo "\n\n\n\n<th>Visits</th>";
			echo "\n\n\n</tr>";
			echo "\n\n</thead>";
			
			echo "\n\n<tbody>";

			    foreach($result as $r) {
			    	
			    	//$string = (strlen($r->url) > 110) ? substr($r->url,0,107).'...' : $r->url;
			    	echo "<tr>"."<td><a href=./". drupal_lookup_path('alias',"node/".$r->nid).">". $r->title."</a></td>"."<td>".$r->times." times"."</td>"."</tr>";
			    }
			

			echo "\n\n</tbody>";
			
			echo "\n</table>";
		echo "</div>"; 		




		// Documents Downloaded

		$result_dl = db_query("
		  	SELECT
			  d.name, d.url, n.title, d.count
			FROM
			  {pubdlcnt} d, {node} n where n.nid = d.nid AND d.uid = :uid
			ORDER BY
			  d.count desc
			",
			array(':uid'=>$_GET['uid'])) -> fetchAll();
		        
       
		foreach($result_dl as $r) {

	    	$counter ++;
	    	$total_downloads += $r->count;
	    }



		/* Download Numbers PIE Graph */	
		echo "<div class='data-container'>";

			echo "<div class='left sharewidth'>";

				echo "<h2>Documents Downloaded</h2>";
				echo "<span> Total Documents downloaded: </span><span class='content-span'>".$counter."</span><br />";
				echo "<span> Total Downloaded Count:  </span><span class='content-span'>".$total_downloads."</span><br />";

				echo "<br />";
				echo "<div id='page-wrap' class='document_scroll'>"; 

					echo "\n<table id='hor-minimalist-c'>";
					
					echo "\n\n<thead>";
						echo "\n\n\n<tr>";
							echo "\n\n\n\n<th>Document Name</th>";
							echo "\n\n\n\n<th>Page</th>";
							echo "\n\n\n\n<th>Type</th>";
							echo "\n\n\n\n<th>Count</th>";
					echo "\n\n\n</tr>";
					echo "\n\n</thead>";
					
					echo "\n\n<tbody>";

					    foreach($result_dl as $r) {

					    	echo "<tr>"."<td>". cut_me_Short (preg_replace("/\\.[^.\\s]{3,4}$/", "", str_replace("_", " ", $r->name)), 45, true)."</td>"."<td>".$r->title."</td>"."<td>".pathinfo($r->name, PATHINFO_EXTENSION)."</td>"."<td>".$r->count."</td>"."</tr>";
				
					    }
					

					echo "\n\n</tbody>";
					
					echo "\n</table>";

				echo "</div>"; 		


			echo "</div>";	

			
			/* <Pie Graph JS */
		    echo "<script type='text/javascript'>";
		    	
		    	echo "function drawVisualization() {";
		        	
		        	echo "var options = { ";
						echo "	  width: 450,";
						echo "    height: 450,";
						echo "    fontName: 'Open Sans',";
						echo "    tooltip: { textStyle: { fontName: 'Tahoma', fontSize: 11 } },";
						echo "    chartArea:{left:10,top:10,width:'100%'},";
						
						echo "    colors: [ '#e5760a', '#d8b71a', '#ad560f', '#1ad8d1', '#193153', '#1d83ae', '#919b02', '#9c2b11',  '#5c6677', '#0fa7ad', '#ad560f', '#d41473' ]";
					echo "};"; 
					//echo "var options = {'title':'asdasdasd asd asd ','width':500,'height':450,'chartArea':{left:0,top:10,width:'100%'}}";
		        	echo "var data = google.visualization.arrayToDataTable([";
		          	echo "['Document', 'No. of Downloads'],";
		        	
		        	foreach($result_dl as $r) {  	
			          	$string = (strlen($r->name) > 55) ? substr($r->name,0,52).'...' : $r->name;
			          	$resultstr_documents_downloaded[] = "['".preg_replace("/\\.[^.\\s]{3,4}$/", "", str_replace("_", " ", $string))."', ".$r->count."]";
			         }
			         echo implode("," , $resultstr_documents_downloaded);
		        echo "]);";
		      
		        
		        echo "new google.visualization.PieChart(document.getElementById('visualization')).";
		            echo "draw(data, options); ";
		     echo " }";
		      

		      echo "google.setOnLoadCallback(drawVisualization);";
		    echo "</script>";
		    /* </Pie Graph JS */

			echo "<div class='right'>";
				
				echo "<div id='holder'>";

					/* Pie Graph Code */
					echo "<div id='visualization'></div>";	
				
				echo "</div>";

			echo "</div>";
			echo "<div class='clear'></div>";
		echo "</div>";



		// Documents Emailed

		$result_em = db_query("
		  	SELECT
			  d.name, d.url, n.title, d.count
			FROM
			  {pubdlcnt} d, {node} n where n.nid = d.nid AND d.uid = :uid
			",
			array(':uid'=>$_GET['uid'])) -> fetchAll();
		        
       
		foreach($result_em as $r) {

	    	$counter ++;
	    	$total_downloads += $r->count;
	    }


		/* Emailed Numbers PIE Graph */	
		echo "<div class='data-container' id='profile-info'>";

			echo "<div class='left sharewidth'>";

				echo "<h2>Documents Emailed (Disabled)</h2>";
				echo "<span> Total Documents Emailed: </span><span class='content-span'>N/A</span><br />";
				echo "<span> Total Emails Sent:  </span><span class='content-span'>N/A</span><br />";

				echo "<br />";
				echo "<div id='page-wrap' class='document_scroll'>"; 

					echo "\n<table id='hor-minimalist-c'>";
					
					echo "\n\n<thead>";
						echo "\n\n\n<tr>";
							echo "\n\n\n\n<th>Document Name</th>";
							echo "\n\n\n\n<th>Page</th>";
							echo "\n\n\n\n<th>Count</th>";
					echo "\n\n\n</tr>";
					echo "\n\n</thead>";
					
					echo "\n\n<tbody>";

					    foreach($result_em as $r) {

					    	echo "<tr>"."<td>".cut_me_Short (preg_replace("/\\.[^.\\s]{3,4}$/", "", str_replace("_", " ", $r->name)), 48, true)."</td>"."<td>".$r->title."</td>"."<td>".$r->count."</td>"."</tr>";
				
					    }
					

					echo "\n\n</tbody>";
					
					echo "\n</table>";

				echo "</div>"; 		


			echo "</div>";	

			
			/* <Pie Graph JS for Email */
		    echo "<script type='text/javascript'>";
		    	
		    	echo "function drawVisualization_for_email() {";
		        	
		        	echo "var options = { ";
						echo "	  width: 450,";
						echo "    height: 450,";
						echo "    fontName: 'Open Sans',";
						echo "    tooltip: { textStyle: { fontName: 'Tahoma', fontSize: 11 } },";
						echo "    chartArea:{left:10,top:10,width:'100%'},";
						//echo "	  title: 'Toppings I Like On My Pizza',";
						//echo "    colors: ['#9c2b11', '#e5760a', '#1d83ae', '#193153', '#919b02' ]";
						echo "    colors: ['#dddddd' ]";
					echo "};"; 
					//echo "var options = {'title':'asdasdasd asd asd ','width':500,'height':450,'chartArea':{left:0,top:10,width:'100%'}}";
		        	echo "var data = google.visualization.arrayToDataTable([";
		          	echo "['Document', 'No. of Emails'],";
		        	
		        	foreach($result_em as $r) {  	
			          	$string = (strlen($r->name) > 55) ? substr($r->name,0,52).'...' : $r->name;
			          	$resultstr_documents_emailed[] = "['".preg_replace("/\\.[^.\\s]{3,4}$/", "", str_replace("_", " ", $string))."', ".$r->count ."]";
			         }
			         echo implode("," , $resultstr_documents_emailed);
		        echo "]);";
		      
		        
		        echo "new google.visualization.PieChart(document.getElementById('visualization_email')).";
		            echo "draw(data, options); ";
		     echo " }";
		      

		      echo "google.setOnLoadCallback(drawVisualization_for_email);";
		    echo "</script>";
		    /* </Pie Graph JS */

			echo "<div class='right'>";
				
				echo "<div id='holder'>";

					/* Pie Graph Code */
					echo "<div id='visualization_email'></div>";	
				
				echo "</div>";

			echo "</div>";
			echo "<div class='clear'></div>";

			echo "<hr />";
			echo "<span><a href=".$GLOBALS['base_url'] ."/"."user-statistics>&larr; Back to Main Analytics Page</a></span>";
		echo "</div>";


	  	//echo "<br />"."Total number of Visits on all Pages: " . "1234" . "<br />";


	}

	else {

		$result_at_glance = db_query("
		  	SELECT
			  DISTINCT(n.title), n.nid, a.totalcount, a.daycount
			  
			FROM
			  {node_counter} a, {node_view_count} b, {node} n where a.nid = n.nid AND n.nid <> 83
			LIMIT 15
			")-> fetchAll();

		// 83 is the analytics page.
		$result_top_pages = db_query("
		  	SELECT
			  n.title, n.nid,
			  COUNT(*) AS times
			FROM
			  {node_view_count} a, {node} n where a.nid = n.nid AND n.nid <> 83 
			GROUP BY
			  n.title
			ORDER BY
			  times desc
			LIMIT 10
			  ")-> fetchAll();

		$result_top_visitors = db_query("
		  	SELECT
			  f.field_full_n_value, u.name, n.nid,
			  COUNT(*) AS times
			FROM
			  {node_view_count} a, {node} n, {users} u, {field_data_field_full_n} f where a.nid = n.nid AND u.uid = a.uid AND f.entity_id = u.uid and u.uid NOT IN (1,2,3,6,7,9,17,35,75,1133)
			GROUP BY
			  u.name
			ORDER BY
			  times desc") -> fetchAll();


		$result_all_users = db_query("
		  	SELECT
			  f.field_full_n_value as title, u.name , u.mail, u.created, u.access, u.uid, COUNT(a.uid) AS times
			FROM
			  {users} u, {field_data_field_full_n} f, {node_view_count} a where f.entity_id = u.uid and a.uid = u.uid and u.uid NOT IN (1,2,3,6,7,9,17,35,75,1133)
			GROUP BY
			  u.name
			ORDER BY
			  times desc") -> fetchAll();
			  //u.access desc") -> fetchAll();
		

		// Show Top Pages & Top Visitors Pie Chart in the first Row
		echo "<div class='data-container'>";

			echo "<h1>At a Glance</h1><br />";



			/* <Area Graph JS for At a Glance */
		    echo "<script type='text/javascript'>";
		    	
		    	echo "function drawVisualization_for_glance() {";
		        	
		        	echo "var options = { ";
						echo "	  width: 968,";
						echo "    height: 400,";
						echo "    fontName: 'Open Sans',";
						echo "    tooltip: { textStyle: { fontName: 'Tahoma', fontSize: 11 } },";
						echo "    chartArea:{width:'82%'},";
						//echo " title: 'Recent Visits',";
						
						echo "    colors: ['#A2AD00', '#9c2b11', '#e5760a', '#1d83ae', '#193153', '#919b02' ],";
						//echo "	  hAxis: {title: 'Pages',  titleTextStyle: {color: 'blue'}}";
						echo "	  vAxis: {title: 'No. of Visits',  titleTextStyle: {color: 'olive'}}";
					echo "};"; 
					//echo "var options = {'title':'asdasdasd asd asd ','width':500,'height':450,'chartArea':{left:0,top:10,width:'100%'}}";
		        	echo "var data = google.visualization.arrayToDataTable([";
		          	// echo "['Day', 'Visits'],";
		        	
		        	$resultstr_at_a_glance[] = "['Pages', 'No. of Visits']";
		        	foreach($result_at_glance as $r) {  
			          	
			          	$resultstr_at_a_glance[] = "['".$r->title."', ".$r->totalcount."]";
			         }
			         echo implode(",",$resultstr_at_a_glance);
		        
		        echo "]);";
		      
		        
		        echo "new google.visualization.ColumnChart(document.getElementById('visualization_glance')).";
		            echo "draw(data, options); ";
		     echo " }";
		      

		      echo "google.setOnLoadCallback(drawVisualization_for_glance);";
		    echo "</script>";
		    /* </Pie Graph JS */

			echo "<div class='right'>";
				echo "<div id='holder'>";

					/* Pie Graph Code */
					echo "<div id='visualization_glance'></div>";	
				
				echo "</div>";

			echo "</div>";
			echo "<div class='clear' id='below-space'></div>";



			/* <Pie Graph - Top Visits */
		    echo "<script type='text/javascript'>";
		    	
		    	echo "function drawVisualization_pages_top() {";
		        	
		        	echo "var options = { ";
						echo "	  width: 450,";
						echo "    height: 450,";
						echo "    fontName: 'Open Sans',";
						echo "    tooltip: { textStyle: { fontName: 'Tahoma', fontSize: 11 } },";
						echo "    chartArea:{left:7,top:5, width:'100%'},";
						echo "    colors: ['#0065BD', '#7D9AAA', '#D4BA00', '#A2AD00', '#00ACED', '#666666', '#E37222', '#CD202C', '#AD005B', '#66307C', '#009AA6', '#006699', '#B5B38C', '#939D98' ]";
						
					echo "};"; 
					//echo "var options = {'title':'asdasdasd asd asd ','width':500,'height':450,'chartArea':{left:0,top:10,width:'100%'}}";
		        	echo "var data = google.visualization.arrayToDataTable([";
		          	echo "['Top Pages', 'No. of Visits'],";
		        	
		        	foreach($result_top_pages as $r) { 
		        		$resultstr_pages_top[] = "['".$r->title."', ".$r->times."]";
			         }
			         echo implode("," , $resultstr_pages_top);
		        echo "]);";
		      	

		      
		        
		        echo "new google.visualization.PieChart(document.getElementById('visualization_top_visits')).";
		            echo "draw(data, options); ";
		     echo " }";
		      

		      echo "google.setOnLoadCallback(drawVisualization_pages_top);";
		    echo "</script>";
		    /* </Pie Graph JS */

			echo "<div class='left' style='width: 45%';>";
				echo "<h3>Top 10 Pages</h3>";
				echo "<div id='holder' class='push-down'>";

					/* Pie Graph Code */
					echo "<div id='visualization_top_visits'></div>";	
				
				echo "</div>";

			echo "</div>";


			/* <Pie Graph JS */
		    echo "<script type='text/javascript'>";
		    	
		    	echo "function drawVisualization_pages_visited() {";
		        	
		        	echo "var options = { ";
						echo "	  width: 450,";
						echo "    height: 450,";
						echo "    fontName: 'Open Sans',";
						echo "    tooltip: { textStyle: { fontName: 'Tahoma', fontSize: 11 } },";
						echo "    chartArea:{left:7,top:5, width:'100%'},";
						echo "    colors: ['#d8b71a', '#193153', '#9c2b11', '#e5760a', '#1d83ae', '#919b02', '#097092', '#42239b', '#ddb928', '#890c0c', '#5c6677', '#0fa7ad', '#ad560f', '#d41473' ]";
						
					echo "};"; 
					//echo "var options = {'title':'asdasdasd asd asd ','width':500,'height':450,'chartArea':{left:0,top:10,width:'100%'}}";
		        	echo "var data = google.visualization.arrayToDataTable([";
		          	echo "['Pages Visited', 'No. of Visits'],";
		        	
		        	foreach($result_top_visitors as $r) {  	
			          	$resultstr_pages_visited[] =  "['".$r->field_full_n_value."', ".$r->times."]";
			         }
			         echo implode("," , $resultstr_pages_visited);

		        echo "]);";
		      
		        
		        echo "new google.visualization.PieChart(document.getElementById('visualization_visit_graph')).";
		            echo "draw(data, options); ";
		     echo " }";
		      

		      echo "google.setOnLoadCallback(drawVisualization_pages_visited);";
		    echo "</script>";
		    /* </Pie Graph JS */


			echo "<div class='right' style='width: 45%';>";
				echo "<h3>Top Visitors</h3>";
				echo "<div id='holder' class='push-down'>";

					/* Pie Graph Code */
					echo "<div id='visualization_visit_graph'></div>";	
				
				echo "</div>";

			echo "</div>";

			echo "<div class='clear' id='push-up'></div>";

		echo "</div>";	



		echo "<h3 class='analytics-heading'>Registered Users ( Total active registered members: ". count($result_all_users)." )</h3><br />";
		echo "<div id='page-wrap' class='content_scroll'>"; 
			echo "\n<table id='hor-minimalist-b'>";
			
			echo "\n\n<thead>";
			echo "\n\n\n<tr>";
			echo "\n\n\n\n<th>Name</th>";
			echo "\n\n\n\n<th>Username</th>";
			echo "\n\n\n\n<th>Registered On</th>";
			echo "\n\n\n\n<th>Last Login</th>";
			echo "\n\n\n</tr>";
			echo "\n\n</thead>";
			
			echo "\n\n<tbody>";

			    
			    foreach($result_all_users as $r) {

			    	$rdate = date('Y-m-d H:i:s', $r->created);
					$dtz = 'America/New_York';
					$regTimestamp = getNoteDateTimeZone($rdate, 'US/Central', $dtz);

					$ldate = date('Y-m-d H:i:s', $r->access);
					$loginTimestamp = getNoteDateTimeZone($ldate, 'US/Central', $dtz);

			    	
			    	
			    	echo "<tr>".
			    	"<td><a href =".$path_alias."?uid=".$r->uid.">".$r->title."</a></td>".
			    	"<td>".$r->name."</td>".
			    	"<td>".date("F jS, Y  \( h:i a \)", strtotime($regTimestamp))."</td>".
			    	"<td>".date("F jS, Y  \( h:i a \)", strtotime($loginTimestamp))."</td>".
			    	"</tr>";
			    }
			

			echo "\n\n</tbody>";
			
			echo "\n</table>";
		echo "</div>";
		echo "<br /><br /><br />"; 		    
	

	}
}
function getBrowser($useragent) {

    // if (isset($_SERVER["HTTP_USER_AGENT"]) OR ($_SERVER["HTTP_USER_AGENT"] != "")) {
    //     $visitor_user_agent = $_SERVER["HTTP_USER_AGENT"];
    // } else {
    //     $visitor_user_agent = "Unknown";
    // }
    
    // check which browser you're using....
	
	if (preg_match('|MSIE ([0-9].[0-9]{1,2})|',$useragent,$matched)) {
		 $browser_version=$matched[1];
		 $browser = 'IE';
		 $icon =  $GLOBALS['base_url'] ."/". path_to_theme().'/images/icons/ie.png';
	 } elseif (preg_match('|Opera/([0-9].[0-9]{1,2})|',$useragent,$matched)) {
		 $browser_version=$matched[1];
		 $browser = 'Opera';
		 $icon =  $GLOBALS['base_url'] ."/". path_to_theme().'/images/icons/opera.png';
	 } elseif(preg_match('|Chrome/([0-9\.]+)|',$useragent,$matched)) {
		 $browser_version=$matched[1];
		 $browser = 'Chrome';
		 $icon =  $GLOBALS['base_url'] ."/". path_to_theme().'/images/icons/chrome.png';
	 } elseif(preg_match('|Firefox/([0-9\.]+)|',$useragent,$matched)) {
		 $browser_version=$matched[1];
		 $browser = 'Firefox';
		 $icon =  $GLOBALS['base_url'] ."/". path_to_theme().'/images/icons/firefox2.png';
	 } elseif(preg_match('|Version/([0-9\.]+)|',$useragent,$matched)) {
		/*
		matches chrome and safari since they are basically the same,
		you can add a check for chrome by simply copy pasting
		and typing 'chrome' rather than safari
		*/
		 $browser_version=$matched[1];
		 $browser = 'Safari';
		 $icon =  $GLOBALS['base_url'] ."/". path_to_theme().'/images/icons/safari.png';
	 } else {
		 // browser not recognized!
		 $browser_version = 0;
		 $browser= 'other';
		 $icon = $GLOBALS['base_url'] ."/". path_to_theme().'/images/icons/unknown.png';
	}
	 //os
	 if (strstr($useragent,'Win')) {
	 	if (strstr($useragent,'Tablet'))
	 		$os='Windows';
	 	else
	 		$os='Windows PC';
	 	$os_icons = $GLOBALS['base_url'] ."/". path_to_theme().'/images/icons/windows3.png';
	 } else if ((strstr($useragent,'Mac')) && (!(strstr($useragent,'Mobile')))) {
	 	$os='Mac';
	 	$os_icons = $GLOBALS['base_url'] ."/". path_to_theme().'/images/icons/mac5.png';
	 } else if ((strstr($useragent,'iPad')) && (strstr($useragent,'Mobile'))) {
	 	$os='iPad';
	 	$os_icons = $GLOBALS['base_url'] ."/". path_to_theme().'/images/icons/ipad4.png';
	 } else if (strstr($useragent,'Linux')) {
	 	$os='Linux';
	 	$os_icons = $GLOBALS['base_url'] ."/". path_to_theme().'/images/icons/linux.png';
	 } else if (strstr($useragent,'Unix')) {
	 	$os='Unix';
	 	$os_icons = $GLOBALS['base_url'] ."/". path_to_theme().'/images/icons/linux.png';
	 } else {
	 	$os='Other';
	 	$os_icons = $GLOBALS['base_url'] ."/". path_to_theme().'/images/icons/unknown.png';
	 }

	 return array(
        'name' => $browser,
        'icon' => $icon,
        'version' => $browser_version,
        'os' => $os,
        'os_icons' => $os_icons
    );
}

function cut_me_Short($string, $length, $stopanywhere=false) {
    //truncates a string to a certain char length, stopping on a word if not specified otherwise.
    if (strlen($string) > $length) {
        //limit hit!
        $string = substr($string,0,($length -3));
        if ($stopanywhere) {
            //stop anywhere
            $string .= '...';
        } else{
            //stop on a word.
            $string = substr($string,0,strrpos($string,' ')).'...';
        }
    }
    return $string;
}

function getNoteDateTimeZone($date = null, $from_dtz = 'US/Central', $to_dtz = null) {
    //$from_zt = 'US/Central'; // Time Zone = -06:00
    if (is_null($date) == FALSE && is_null($from_dtz) == FALSE && is_null($to_dtz) == FALSE) {
        // set TimeZone from
        $time_object = new DateTime($date, new DateTimeZone($from_dtz));
        $time_now_object = new DateTime("now", new DateTimeZone($from_dtz));
        // Change TimeZone
        $time_object->setTimezone(new DateTimeZone(trim($to_dtz)));
        $time_now_object->setTimezone(new DateTimeZone(trim($to_dtz)));
        // Is day = day in $time_now_object, $time_object..?
        if ($time_now_object->format('d') == $time_object->format('d')) {
            return $time_object->format('H:i:s');
        } else {
            return $time_object->format('Y-m-d H:i:s');
        }
    } else {
        return '';
    }
}

?>
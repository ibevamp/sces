
$.noConflict();

jQuery(document).ready(function($) {

	// Enable console.log fallback for IE
	var alertFallback = false; // set to true to get console messages as alerts in IE
	if (typeof console === "undefined" || typeof console.log === "undefined") {
	 console = {};
	 if (alertFallback) {
	     console.log = function(msg) {
	          alert(msg);
	     };
	 } else {
	     console.log = function() {};
	 }
	}
	
	// Remove the right border from the last item in the nav
	$("#access .menu li:last-child, .menu li:last-child, .sub-menu li:last-child").css("border-right", "none");
	$("aside:last-child").css("border-bottom", "none");
	
	// Make the expandable sections open and close when h3.expandable is clicked
	$('.expandable div.extra').hide();
	$('.expandable h3').addClass('expandable');
	$('.expandable h3').click(function() {
		$(this).parent().find('div').animate({height: "toggle", opacity: "toggle"});
		
		// If the arrow is closed, open it or vice versa
		if($(this).hasClass('open')) {
			$(this).removeClass('open').addClass('closed');
		}
		else {
			$(this).removeClass('closed').addClass('open');
		}
	});
	
	// Make the 'details' div of articles open and close when h3.expandable is clicked
	$('.exp div.extra').hide();
	$('.exp h3').addClass('expandable');
	$('.exp h3').click(function() {
		$(this).parent().find('div').animate({height: "toggle", opacity: "toggle"});
		
		// If the arrow is closed, open it or vice versa
		if($(this).hasClass('open')) {
			$(this).removeClass('open').addClass('closed');
		}
		else {
			$(this).removeClass('closed').addClass('open');
		}
	});
	
	// Initialize fancybox links
	$.fancybox.init(); 
	$(".iframe").each(function(i){
	    var iFrameHeight = $(this).attr('class').match(/iframeheight\S+/);
		
		if(iFrameHeight) {
			iFrameHeight = parseInt(iFrameHeight[0].split('iframeheight')[1]);
		} else {
			iFrameHeight = 438;
		}
		
		var iFrameWidth = $(this).attr('class').match(/iframewidth\S+/);
		
		if(iFrameWidth) {
			iFrameWidth = parseInt(iFrameWidth[0].split('iframewidth')[1]);
		} else {
			iFrameWidth = 577;
		}
		
		$(this).fancybox({
			'width'			: iFrameWidth,
			'height'		: iFrameHeight,
	        'transitionIn'	: 'elastic',
			'transitionOut'	: 'elastic',
			'scrolling'		: 'no',
      'autoScale'     : false,
      'type'          : 'iframe'
		});
	});
	
	// Create a sticky widget 
	var STICKY = {
	
		// Start a timer on window scroll to prevent firing lots of extra functions
		timer : setTimeout(function(){}),
		
		// Make an element sticky (position: fixed) and show it
		makeSticky : function (element, bottom){
			$element = $(element)
			parentHeight = $element.parent()[0] ? $element.parent().height() : 0
			parentTop = $element.parent()[0] ? $element.parent().offset().top : 0
			windowTop = $(window).scrollTop()
			windowHeight = $(window).height()
			
			if($element.css('display') == 'none' && windowTop > parentTop ) {
				$element.addClass("fixed").css({"position": "fixed"}).fadeIn(200) 
			}
			else if(windowTop <= parentTop) {
				$element.hide()
			}
			if(bottom && windowTop + windowHeight >= $(document).height()){
				$element.hide()
			}
		},
		
		scrollCheck : function () {
			clearTimeout(STICKY.timer)
			STICKY.timer = setTimeout(function(){
				STICKY.makeSticky("#back-to-top")
				STICKY.makeSticky("#back-to-top-2", true)
				STICKY.makeSticky("#sticky-nav-clone")
			}, 50)
		}
		
	}
	// Only do the following if the browser isn't IE 6
	if(!$('html.ie6')[0]) {
		
		// Add 'back to top' links
		$('#page')
			.append('<div id="back-to-top"><a href="#">Back to top</a></div>')
			.append('<div id="back-to-top-2"><a href="#">Back to top</a></div>')
		
		$('#back-to-top').hide()
			.css({
				'top': '0px'
			})
		
		$('#back-to-top-2').hide()
			.css({
				'bottom': '0px'
			})
		
		// Clone the sticky nav
		$('#sticky-nav').parent().append($('#sticky-nav').clone().attr('id', 'sticky-nav-clone').hide())

		// use a timeout function to avoid lots of calls when scrolling
		$(window).scroll(STICKY.scrollCheck)
	}
	
	// Add hover states to image maps with the data-hover-img attribute
	var IMAGEMAPHOVERS = {
		init: function() {
			var maps = $('map[data-hover-img]')
			if(maps[0]) {
				maps.each(function() {
					$(this).data('hoverimg', $(this).attr('data-hover-img')) // store a reference to the hover image
					$(this).data(
						'mapholder', 
						$('img[usemap=#' + $(this).attr('name') + ']')
						.css({'margin': 0})
						.wrap('<div class="img-map-holder" style="position: relative" />')
						.parent()) // store a reference to the image map holder
					$(this).find('area[coords]').each(function() {
						var $t = $(this)
						var $p = $t.parent()
						var coords = $t.attr('coords').replace(/ /g,'').split(',') //strip spaces and convert to an array
						var href = $t.attr('href')
						var holder = $p.data('mapholder')
						var hoverdiv = $('<div class="map-area-hover"><a href="' + href + '" style="display:block; width: 100%; height: 100%;"></a></div>').css({
								'position': 'absolute',
								'left': parseInt(coords[0]),
								'top': parseInt(coords[1]),
								'width': coords[2] - coords[0],
								'height': coords[3] - coords[1],
								'background-image': 'url(' + $p.data('hoverimg') + ')',
								'background-repeat': 'no-repeat',
								'background-position': -coords[0] + 'px ' + -coords[1] + 'px',
								'opacity': '0'
							})
						holder.append(hoverdiv)
					})
				})
				$('.map-area-hover').hover(
					function() {
						$(this).stop().animate({ opacity: 1}, 300)
					},
					function() {
						$(this).stop().animate({ opacity: 0}, 500)
					}
				)
			}
		}
	} 
	
	/* initialize image map hovers */
	IMAGEMAPHOVERS.init()
	
	/* Animate transitions to links on the same page */	
	var ANIMATEANCHORS = {
		init: function() {
			$('a[href^=#], area[href^=#]').not('.fancybox, .iframe').on("click", function() {
				if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
					var $target = $(this.hash)
					$target = $target.length && $target || $('[name=' + this.hash.slice(1) +']')
					var targetOffset = $target.length ? $target.offset().top - 40 : 0 // Offset the target distance to have some margin at the top
					
					// detect iOS
					var deviceAgent = navigator.userAgent.toLowerCase();
					$iOS = deviceAgent.match(/(iphone|ipod|ipad)/);
					positionFixed();
					
					$('html, body').stop().animate({scrollTop: targetOffset}, 700, function () {
    				// make fixed position elements absolute for iOS
    				if ($iOS) {
	    				$('#sticky-nav-clone, #back-to-top').css({position:'absolute',top:targetOffset})
	    				$('#back-to-top-2').css({
	    					position:'absolute'
	    					, top:targetOffset + $(window).height()
	    					, bottom: 'auto'
	    				})
	    			}
					})
					return false // prevent default
					}
			})
		}
	}
	    
	/* Put this code after all anchor links have been added to the page */
  ANIMATEANCHORS.init();	
	
	/* set position fixed for iOS anchor link bug fix */
	$(document).bind('touchmove', positionFixed);
	function positionFixed(){
		$('#sticky-nav-clone, #back-to-top').css({position:'fixed',top:0})
	 	$('#back-to-top-2').css({position:'fixed', top:'auto', bottom: 0})
	}
});




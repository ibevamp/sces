jQuery(document).ready(function() {

	
	if($("#jquery_jplayer_1")[0] && jplayerparameters && videoparameters) {
	  
	  $("#jquery_jplayer_1").jPlayer({
		  ready: function () {
			  $(this).jPlayer("setMedia", {
				  m4v: jplayerparameters.m4v, 
			  	flv: videoparameters.src, 
				  poster: jplayerparameters.poster
			  });
		  },
		  swfPath: "files/flv",
		  solution:"flash,html",
		  supplied: 'flv, m4v',
	 
		  size: {
			  width: jplayerparameters.width || "544px",
			  height: jplayerparameters.height || "308px",
			  cssClass: "jp-video"
		  },
		  
		  canplay: function() { $("#jquery_jplayer_1").jPlayer("play") },
		  
		  error: function (event) { 
			  if (event.jPlayer.error.type == "e_no_solution") {
				  $("#backup-video").show();
				  $("#jp_container_1").hide();
				  swfobject.embedSWF(
					"https://apps.mckinsey.com/t30/sites/all/themes/whymck/video/StrobeMediaPlayback.swf"
					, "StrobeMediaPlayback"
					, videoparameters.width || 544
					, videoparameters.height || 308
					, "0.0.0"
					, "expressInstall.swf"
					, videoparameters
					, {
						allowFullScreen: "true",
						allowscriptaccess:'always',
						wmode: "transparent"
					}
					, {
						name: "StrobeMediaPlayback"
					}
				);
			  }
		  }
	  })
	
	  $("#jquery_jplayer_1").bind($.jPlayer.event.click, function(e){
		  // Add a loading event listener that shows the loading spinner
		  $(".spinner").show();
		  Spinners.create(".spinner", {
			radii:     [15, 30],
			color:     '#777',
			dashWidth: 3,
			dashes:    20,
			opacity:   1,
			speed:     .7
		  }).play();
	  });
	  
	  $("#jquery_jplayer_1").bind($.jPlayer.event.play, function(e){
		  // Remove the spinner
		  $(".spinner").hide();
		  Spinners.get('.spinner').remove();
	  });
	}
	else if(videoparameters) {
		swfobject.embedSWF(
			"https://apps.mckinsey.com/t30/sites/all/themes/whymck/video/StrobeMediaPlayback.swf"
			, "StrobeMediaPlayback"
			, videoparameters.width || 546
			, videoparameters.height || 308
			, "0.0.0"
			, "expressInstall.swf"
			, videoparameters
			, {
	            allowFullScreen: "true",
	            allowscriptaccess:'always',
	            wmode: "transparent"
	        }
			, {
	            name: "StrobeMediaPlayback"
	        }
		);
	}




	/* Second Video Parameters*/

	if($("#jquery_jplayer_2")[0] && jplayerparameters2 && videoparameters2) { 
	  $("#jquery_jplayer_2").jPlayer({
		  ready: function () {
			  $(this).jPlayer("setMedia", {
				  m4v: jplayerparameters2.m4v, 
			  	flv: videoparameters2.src, 
				  poster: jplayerparameters2.poster
			  });
		  },
		  swfPath: "files/flv",
		  solution:"flash,html",
		  supplied: 'flv, m4v',
	 
		  size: {
			  width: jplayerparameters2.width || "544px",
			  height: jplayerparameters2.height || "308px",
			  cssClass: "jp-video"
		  },
		  
		  canplay: function() { $("#jquery_jplayer_2").jPlayer("play") },
		  
		  error: function (event) { 
			  if (event.jPlayer.error.type == "e_no_solution") {
				  $("#backup-video2").show();
				  $("#jp_container_2").hide();
				  swfobject.embedSWF(
					"https://apps.mckinsey.com/t30/sites/all/themes/whymck/video/StrobeMediaPlayback.swf"
					, "StrobeMediaPlayback"
					, videoparameters2.width || 544
					, videoparameters2.height || 308
					, "0.0.0"
					, "expressInstall.swf"
					, videoparameters2
					, {
						allowFullScreen: "true",
						allowscriptaccess:'always',
						wmode: "transparent"
					}
					, {
						name: "StrobeMediaPlayback"
					}
				);
			  }
		  }
	  })
	
	  $("#jquery_jplayer_2").bind($.jPlayer.event.click, function(e){
		  // Add a loading event listener that shows the loading spinner
		  $(".spinner").show();
		  Spinners.create(".spinner", {
			radii:     [15, 30],
			color:     '#777',
			dashWidth: 3,
			dashes:    20,
			opacity:   1,
			speed:     .7
		  }).play();
	  });
	  
	  $("#jquery_jplayer_2").bind($.jPlayer.event.play, function(e){
		  // Remove the spinner
		  $(".spinner").hide();
		  Spinners.get('.spinner').remove();
	  });
	}


});

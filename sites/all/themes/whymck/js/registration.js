jQuery(document).ready(function($){

var attend = $("#edit-submitted-event-1");
attend.change(function () {
  if(attend.prop('checked')) {
    $("div[id^=webform-component-business]:not(#webform-component-business-e-mail-address)").fadeIn();
    $("div[id^=webform-component-business] label").fadeIn();
    $("div[id^=webform-component-business] input").fadeIn();
    $("div[id^=webform-component-assistants]").fadeIn();
    $("#webform-component-accomodations, #webform-component-hotel-reservation, #webform-component-transportation, #webform-component--please-note-that-you-will-be-responsible-for-your-transportation-to-and-from-the-airport, #webform-component-dietary-requirements-etc, #webform-component-dietary-restrictions, #webform-component-additional-comments").fadeIn();
    $("#webform-component-assistants-name, #webform-component-assistants-phone, #webform-component-assistants-e-mail-address").andSelf().children().fadeIn();
      } else {
    $("div[id^=webform-component-business]:not(#webform-component-business-e-mail-address)").fadeIn();
    $("div[id^=webform-component-business]:not(#webform-component-business-e-mail-address) label").fadeOut();
    $("div[id^=webform-component-business]:not(#webform-component-business-e-mail-address) input").fadeOut();
    $("div[id^=webform-component-assistants]").fadeOut();
    $("#webform-component-accomodations, #webform-component-hotel-reservation, #webform-component-transportation, #webform-component--please-note-that-you-will-be-responsible-for-your-transportation-to-and-from-the-airport, #webform-component-dietary-requirements-etc, #webform-component-dietary-restrictions, #webform-component-additional-comments").fadeOut();
    $("#form#webform-client-form-9 div.webform-component-textfield input").fadeOut();

  }
});

});

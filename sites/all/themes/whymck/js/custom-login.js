//jQuery(document).ready(function($) { 
jQuery(function($) {
	$("body").addClass("display-none");
	var head = document.getElementsByTagName("head")[0];
	if (head) {

		//alert("custom-login.js")
		if (window.PIE) {
	        $('.rounded').each(function() {
	            PIE.attach(this);
	        });
	    }

		$("#edit-name").val("");

		$('#edit-pass').attr("placeholder","Passcode");
		$('#edit-submit').val("Submit");
		$("#edit-name").hide();

		$('.page-toboggan-denied #page #user-login .form-submit').show();
		
		$("div.description").text("Please refer to your email for your passcode.");

		$("body").addClass("login-authentic");

		$('.block-inner .content #footer').find('a', '|').remove();
		$('.block-inner .content').find('ul').remove();
		// $('.block-inner .content').find('img').remove();

		/* <img id="mck-logo" width="195" height="21" src="sites/all/themes/whymck/images/logo_mck.png"> */

		$("body").removeClass("display-none");
		$("body").removeClass("display-none");
	}
});
jQuery(document).ready(function() {
	var deviceAgent = navigator.userAgent.toLowerCase();


	$iOS = deviceAgent.match(/(iphone|ipod|ipad)/);

	if(videoparameters && $("#StrobeMediaPlayback")[0] && !$iOS) {
		swfobject.embedSWF(
			"sites/all/themes/whymck/videos/StrobeMediaPlayback.swf"
			, "StrobeMediaPlayback"
			, videoparameters.width || 546
			, videoparameters.height || 308
			, "0.0.0"
			, "expressInstall.swf"
			, videoparameters
			, {
	            allowFullScreen: "true",
	            allowscriptaccess:'always',
	            wmode: "transparent"
	        }
			, {
	            name: "StrobeMediaPlayback"
	        }
		)
	}

	else if(jplayerparameters && videoparameters) {
    	$("#jp_container_1").show()
		$("#jquery_jplayer_1").jPlayer({
			  ready: function () {
				  $(this).jPlayer("setMedia", {
					  m4v: jplayerparameters.m4v, 
					  poster: jplayerparameters.poster
				  })
			  },
			  swfPath: "videos",
			  solution:"html",
			  supplied: 'm4v',
		 
			  size: {
				  width: jplayerparameters.width || "544px",
				  height: jplayerparameters.height || "308px",
				  cssClass: "jp-video"
			  },

			  canplay: function() { $("#jquery_jplayer_1").jPlayer("play") },
			  error: function (event) { 
			  	  if (event.jPlayer.error.type == "e_no_solution") {
					  $("#backup-video").show();
					  $("#jp_container_1").hide();
					  swfobject.embedSWF(
						"sites/all/themes/whymck/videos/StrobeMediaPlayback.swf"
						, "StrobeMediaPlayback"
						, videoparameters.width || 544
						, videoparameters.height || 308
						, "0.0.0"
						, "expressInstall.swf"
						, videoparameters
						, {
							allowFullScreen: "true",
							allowscriptaccess:'always',
							wmode: "transparent"
						}
						, {
							name: "StrobeMediaPlayback"
						}
					)
				}
			}
		})
	}
    

  $("#jquery_jplayer_1").bind($.jPlayer.event.click, function(e){
	  // Add a loading event listener that shows the loading spinner
	  $(".spinner").show();
	  Spinners.create(".spinner", {
		radii:     [15, 30],
		color:     '#777',
		dashWidth: 3,
		dashes:    20,
		opacity:   1,
		speed:     .7
	  }).play()
  })
  
  $("#jquery_jplayer_1").bind($.jPlayer.event.play, function(e){
	  // Remove the spinner
	  $(".spinner").hide();
	  Spinners.get('.spinner').remove()
  })
	})
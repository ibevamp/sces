$(document).ready(function(){ 
	
	$('#features').jshowoff({ 
		speed:10000, 
		controlText: { play:'', pause:'', previous:'<img src="sites/all/themes/whymck/images/left.png" alt="next"  />', next:'<img src="sites/all/themes/whymck/images/right.png"  />' },
		effect: "slideLeft"
	})

	var minFeaturesHeight = $('#features').height()
	var headlineHeight = $('#headline').height()
	// add and enable the collapse button
	var timeout
	$('.collapse-bt')
		.html('<span class="collapse">read more</span><span class="expand">read what? why? how?</span>')
		.find('.expand')
		.toggle()
		.parent()
		.on('click', function(e){
			e.preventDefault()
			$(this)
				.blur()
				.toggleClass('expanded')
				.find('.expand')
					.toggle()
			$(this).find('.collapse').toggle()
			$('#features li').slideToggle()
			$('#headline .jshowoff-slidelinks').stop().slideToggle(200)
			$('#headline').stop().animate({
				// Set the height of the headline
				height: $('#headline').height() < headlineHeight ? headlineHeight + 'px' : $('.non-consulting-roles')[0] ? '180px' : '152px'
			}, 500)
			if($(this).hasClass('expanded')) { 
				$().playjshowoff('hover')
			} else { 
				$().pausejshowoff('playBtn')
			}
		})
	$('.jshowoff-slidelinks a').on('click', function(e){
		$(this).blur()
	})

	// Make the expandable sections open and close when h3.expandable is clicked
	// Make the 'details' div of articles open and close when h3.expandable is clicked
	$('.exp div.extra').hide();
	$('.exp h3').addClass('expandable').click(function() {
		$(this).parent().find('div').animate({height: "toggle", opacity: "toggle"});
		
		// If the arrow is closed, open it or vice versa
		$(this).hasClass('open') ? $(this).removeClass('open').addClass('closed') : $(this).removeClass('closed').addClass('open');
	});				
	

	// add and update Cufon text, if in use
	if(typeof Cufon !== "undefined" && Cufon !== null) {
		Cufon.replace('.jshowoff-slidelinks a', { 
			fontFamily: 'helvetica-neue-light'
		})							
		Cufon.replace('#headline .background-title', { 
			fontFamily: 'helvetica-neue-light'
		})							
		Cufon.now()
		$('.jshowoff-slidelinks a').on('click', function(e){
			Cufon.refresh()
		})
		$('.jshowoff-slidelinks a').on('hover', function(e){
			Cufon.refresh()
		})
	} 
	
	// Create a sticky widget 
	STICKY = {}
	
	

		
	// Make an element sticky (position: fixed) and show it
	STICKY.makeSticky = function (element, checkBottom){
		$element = $(element)
		parentHeight = $element.parent()[0] ? $element.parent().height() : 0
		parentTop = $element.parent()[0] ? $element.parent().offset().top : 0
		windowTop = $(window).scrollTop()
		windowHeight = $(window).height()
		
		if(windowTop > parentTop ) {
			$element.addClass("fixed").slideDown(200) 
			$('[data-spy="scroll"]').each(function () {
				$(this).scrollspy('refresh');
			});
		}
		else {
			$element.slideUp(200, function() {
				$(this).removeClass('fixed')
			})
		}
		
		// Hide the element if the bottom of the screen is lower than the parent element
		if(checkBottom) {
			if(windowTop + windowHeight > parentHeight + parentTop ) {
				$element.stop().slideUp(200)
			}
		}
	}

	/* Animate transitions to links on the same page */ 
    var ANIMATEANCHORS = {
        init: function() {
            $('a[href^=#], area[href^=#]').not('.fancybox, .iframe').on("click", function() {
                if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') 
                	&& location.hostname == this.hostname) {
                    
                    var $target = $(this.hash)
                    $target = $target.length && $target || $('[name=' + this.hash.slice(1) +']')
                    
                    // Default anchor offset
                    var topOffset = 60;

                    if (typeof anchorsOffset !== "undefined" && anchorsOffset !== null) {
  						topOffset = anchorsOffset;
					}

                    // Offset the target distance to have some margin at the top
                    var targetOffset = $target.length ? $target.offset().top - topOffset : 0
                    $('html, body').stop().animate({scrollTop: targetOffset}, 700, function () {
                    
                    })
                    return false // prevent default
                    }
            })
        }
    }

    ANIMATEANCHORS.init()

})
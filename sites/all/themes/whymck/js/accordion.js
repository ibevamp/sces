
jQuery(document).ready(function(){
    jQuery('.accordion-toggle').click(function(){

      //Hide the other panels
      jQuery(".accordion-content").not(jQuery(this).parent().find('.accordion-content')).slideUp('fast');

           //Expand or collapse this panel
      jQuery(this).parent().find('.accordion-content').slideToggle('fast', function() {jQuery(this).parent().parent().get(0).scrollIntoView(); 
        if($(window).width() < 1100) {
        $top = $(window).scrollTop(); 
        $(window).scrollTop($top-70);
} else {
        $top = $(window).scrollTop(); 
        $(window).scrollTop($top-110);
}
      });
  // check if it was already active
  if (jQuery(this).hasClass('active')) {
     jQuery('.accordion-toggle').removeClass('active');
  }
  else {
    jQuery('.accordion-toggle').removeClass('active');
    jQuery(this).addClass('active');
  }

  
});});
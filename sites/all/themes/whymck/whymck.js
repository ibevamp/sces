
(function ($) {
		$("body").addClass("display-none");
		//alert("whymck.js");
Drupal.behaviors.whymck = {
attach: function(context, settings) {

  $(".toggler").click(function(){
    $(this).next().slideToggle("slow");

  }).next().hide();

  $("#mck-nav-main").click(function (){
  
  $("#nav-icon3").toggleClass("open");
  $(".mobile-menu-wrap").toggleClass("open");
});

$(".accordion").click(function(){
	$(this).toggleClass("open");
	$(this).find(".internal-content div").slideToggle();
	
	if($(this).hasClass("open")) {
		$(this).find(".mck-icon__carrot-down").hide();
		$(this).find(".mck-icon__carrot-up").show();
	} else {
		$(".accordion span.mck-icon__carrot-down").show();
		$(".accordion span.mck-icon__carrot-up").hide();
	}	
});

$(".expandall").click(function(){
	$(".accordion .internal-content div").slideDown();
	$(this).hide();
	$(".collapseall").show();
	$(".accordion span.mck-icon__carrot-down").hide();
	$(".accordion span.mck-icon__carrot-up").show();
});

$(".collapseall").click(function(){
	$(".accordion .internal-content div").slideUp();
	$(this).hide();
	$(".expandall").show();
	$(".accordion span.mck-icon__carrot-down").show();
	$(".accordion span.mck-icon__carrot-up").hide();
});


} // end attach

};
})(jQuery);    


<div class="<?php print $classes . ' ' . $zebra; ?>">
	<div class="comment-inner">
		

	    
    <h4><strong>By: <?php print $author; ?></strong> on <?php print $created; ?> </h4>
   
    <div class="content">
      <?php 
        hide($content['links']); 
        print render($content);
        ?>
      <?php if ($signature): ?>
        <div class="signature"><?php print $signature ?></div>
      <?php endif; ?>
    </div>
    <br />
    <?php if (!empty($content['links'])): ?>
	    <div class="links"><?php print render($content['links']); ?></div>
	  <?php endif; ?>

  </div> <!-- /comment-inner -->
</div> <!-- /comment -->
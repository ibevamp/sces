  <!-- ______________________ PAGE HEADER _______________________ -->

<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=2.0, user-scalable=yes" />
<header class="default-header clearfix">
  <a href="./" class="default-logo"><img src="https://apps.mckinsey.com/sces/sites/all/themes/whymck/images/semi-icon.png" style=""> </a>
  <nav id="mck-nav-main" class="">
    <div id="nav-icon3">
      <span></span>
      <span></span>
      <span></span>
      <span></span>
    </div>
     <!-- Primary level item only -->
     <div class="mobile-menu-wrap clearfix">
      <?php print theme('links', array('links' => $main_menu, 'attributes' => array('id' => 'primary', 'class' => array('mck-nav-main__list','links', 'clearfix', 'main-menu')))); ?>
    </div>

  </nav>

</header>


<div id="page" class="<?php print $classes; ?>"<?php print $attributes; ?>>


  <!-- ______________________ MAIN _______________________ -->

  <div id="main" class="clearfix">
    <div id="content2">
      <div id="content-inner" class="inner column center">
      <?php if ($tabs): ?>

            <?php endif; ?>
        <?php if ($breadcrumb || $title|| $messages || $action_links): ?>
          <div id="content-header">

            <?php print $breadcrumb; ?>

            <?php if ($page['highlight']): ?>
              <div id="highlight"><?php print render($page['highlight']) ?></div>
            <?php endif; ?>

            <?php if ($title): ?>
              <h1 class="title"><?php print $title; ?></h1>
            <?php endif; ?>

            <?php print $messages; ?>
            <?php print render($page['help']); ?>




          </div> <!-- /#content-header -->
        <?php endif; ?>

        <div id="content-area">
          <?php print render($page['content']) ?>
        </div>

      <div class="tabs"><?php print render($tabs); ?></div>


      </div>
    </div> <!-- /content-inner /content -->


    <?php if ($page['sidebar_second']): ?>
		<style>
  		#content2{
      	width:610px;
      	float:left
      }
    </style>
      <div id="sidebar-second" class="column sidebar second">
        <div id="sidebar-second-inner" class="inner">
          <?php print render($page['sidebar_second']); ?>
        </div>
      </div>
    <?php endif; ?> <!-- /sidebar-second -->

  </div> <!-- /main -->

</div> <!-- /page -->
  <!-- ______________________ FOOTER _______________________ -->
  
<div class="newfooter clearfix">

      <div style="float:left;">  <p style="color:#fff;"><strong>Semiconductor Executive Summit</strong><br/>
McKinsey & Company © 2016. All Rights Reserved. </p></div>
              <div class="footerlinks">

                <a href="">Privacy Policy</a>
                <a href="">Terms and Conditions</a>
              </div>
</div>
<a href="#" id="back-to-top" title="Back to top">&uarr;</a>




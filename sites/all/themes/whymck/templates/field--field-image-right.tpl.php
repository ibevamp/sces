<?php 
	$img = file_create_url($items[0]['#item']['uri']);
	$videourl = $element['#object']->field_video_url['und']['0']['value'];
	$nid = $element['#object']->nid;
	$vidimg = isset($element['#object']->field_video_image['und'][0]['uri']) ? file_create_url($element['#object']->field_video_image['und'][0]['uri']) : NULL;
	
	if(isset($videourl)) { // if a video url is set use the video output below
?>
<video id="video-slider" class="video-js vjs-default-skin video-<?php echo $nid; ?>"
  preload="auto" width="798" height="448"
  poster="<?php echo $vidimg; ?>"
  data-setup='{"example_option":false}'>
 <source src="<?php echo $videourl; ?>" type='video/mp4' />
 <p class="vjs-no-js">To view this video please enable JavaScript, and consider upgrading to a web browser that <a href="http://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a></p>
</video>
<div class="running-time">
<strong>Running Time: </strong><span class="duration-total"></span>
</div>
<?php } else { //if no video url is set display the image from the node ?>

	<div class="field-name-field-image-right" style="background:url(<?php echo $img; ?>) no-repeat center center;"></div>

<?php } ?>
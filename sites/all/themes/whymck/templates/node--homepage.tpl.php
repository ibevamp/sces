<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=2.0, user-scalable=yes" />

<div id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?>">
	<div class="node-inner">
  	<div class="content">
  	  <?php 
  	  	$content['body']['#weight'] = 2;
  	  	$content['field_buckets']['#weight'] = 3;
        hide($content['comments']);
        hide($content['links']);
        print render($content);
       ?>
  	</div>
 	

        
	</div> <!-- /node-inner -->
</div> <!-- /node-->

<!DOCTYPE html>
<!--[if lt IE 7]><html class="lt-ie9 lt-ie8 lt-ie7"><![endif]-->
<!--[if IE 7]><html class="lt-ie9 lt-ie8"><![endif]-->
<!--[if IE 8]><html class="lt-ie9"><![endif]-->
<!--[if gt IE 8]><!--><html><!--<![endif]-->

<head>

<script src="sites/all/themes/whymck/js/excanvas.js"></script>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<?php print $head; ?>
<title><?php 
if ($head_title == 'Access Denied / User log in | SCCD 2015') {
    $head_title = t('Passcode Authentication | SCCD 2015');
  }
  print $head_title; ?></title>
<?php print $styles; ?><?php print $scripts; ?>

<link rel="stylesheet" type="text/css" href="<?php print base_path() . path_to_theme(); ?>/css/styles2.css" />


<link href="<?php print base_path() . path_to_theme(); ?>/skin/dark/jplayer.dark.css" rel="stylesheet" type="text/css" />

<link rel="stylesheet" href="<?php print base_path() . path_to_theme(); ?>/flex/css/flexslider.css" type="text/css" media="screen" />

<script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.5.3/modernizr.min.js"></script>

<script src="<?php print base_path() . path_to_theme(); ?>/flex/js/libs/modernizr-2.5.2-respond-1.1.0.min.js"></script>

<link href="<?php print base_path() . path_to_theme(); ?>/css/responsive.css" rel="stylesheet" type="text/css" />

</head>

<body class="<?php print $classes; ?>" <?php print $attributes;?>>
<div id="skip"> <a href="#main-menu"><?php print t('Jump to Navigation'); ?></a> </div>
<?php print $page_top; ?> <?php print $page; ?> <?php print $page_bottom; ?> 
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script> 

<!-- FlexSlider --> 
<script src="<?php print base_path() . path_to_theme(); ?>/flex/js/jquery.flexslider-min.js"></script> 
<script src="<?php print base_path() . path_to_theme(); ?>/flex/js/script.js"></script> 
<script type="text/javascript" src="<?php print base_path() . path_to_theme(); ?>/js/jquery.jshowoff-custom.js"></script> 


<script type="text/javascript" src="<?php print base_path() . path_to_theme(); ?>/video/jquery.jplayer.min.js"></script> 
<script type="text/javascript" src="<?php print base_path() . path_to_theme(); ?>/video/spinners.min.js"></script> 
<script type="text/javascript" src="<?php print base_path() . path_to_theme(); ?>/js/swfobject.js"></script> 
<script type="text/javascript" src="<?php print base_path() . path_to_theme(); ?>/js/functions.js"></script>

</body>
</html>